﻿namespace Kata2
{
    public class Program
    {


        static void Main(string[] args)
        {
            HumanReadableTime humanReadableTime = new HumanReadableTime();
            humanReadableTime.CalculateHumanReadableTime(0);
            humanReadableTime.CalculateHumanReadableTime(5);
            humanReadableTime.CalculateHumanReadableTime(60);
            humanReadableTime.CalculateHumanReadableTime(86399);
            humanReadableTime.CalculateHumanReadableTime(359999);
        }
    }
}
