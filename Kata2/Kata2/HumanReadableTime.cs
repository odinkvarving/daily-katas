﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata2
{
    internal class HumanReadableTime
    {
        public void CalculateHumanReadableTime(int seconds)
        {
            if (seconds > 359999)
            {
                throw new Exception();
            }
            int hours = seconds / 3600;
            double minutesRemaining = seconds % 3600;
            if (hours > 0)
            {
                int minutes = (int)minutesRemaining / 60;
                double secondsRemaining = seconds % 60;

                if (minutes > 0)
                {
                    seconds = (int)secondsRemaining;
                    string solution = $"{hours:D2}:{minutes:D2}:{seconds:D2}";
                    Console.WriteLine(solution);
                }
                else
                {
                    seconds = (int)secondsRemaining;
                    string solution = $"{hours:D2}:{minutes:D2}:{seconds:D2}";
                    Console.WriteLine(solution);
                }
            }
            else
            {
                int minutes = seconds / 60;
                seconds = seconds % 60;
                string solution = $"{hours:D2}:{minutes:D2}:{seconds:D2}";
                Console.WriteLine(solution);
            }
        }
    }
}
