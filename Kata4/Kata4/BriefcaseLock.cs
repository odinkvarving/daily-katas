﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata4
{
    internal class BriefcaseLock
    {

        public int CalculateTurns(string fromCode, string toCode)
        {
            List<int> fromNumbers = new List<int>();
            foreach (char c in fromCode)
            {
                fromNumbers.Add(c - '0');
            }

            List<int> toNumbers = new List<int>();
            foreach (char c in toCode)
            {
                toNumbers.Add(c - '0');
            }

            int turns = 0;
            for (int i = 0; i < fromNumbers.Count; i++)
            {
                turns += CalculateTurns(fromNumbers[i], toNumbers[i]);
            }

            return turns;
        }

        public int CalculateTurns(int from, int to)
        {
            int backwards;
            int forwards;

            if (to > from)
            {
                backwards = from + 9 - to + 1;
                forwards = to - from;
            }
            else if (to < from)
            {
                backwards = to + 9 - from + 1;
                forwards = from - to;
            }
            else
            {
                backwards = 0;
                forwards = 0;
            }

            if (backwards > forwards)
            {
                return forwards;
            }
            else
            {
                return backwards;
            }
        }
    }
}
