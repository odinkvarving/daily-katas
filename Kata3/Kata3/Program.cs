﻿using Kata3;

namespace kata3
{
    public class Progam
    {
        static void Main(string[] args)
        {
            RemoveWords removeWords = new RemoveWords();
            char[] strings1 = { 's', 't', 'r', 'i', 'n', 'g', 'w' };
            char[] strings2 = { 'b', 'b', 'l', 'l', 'g', 'n', 'o', 'a', 'w' };
            char[] strings3 = { 'a', 'n', 'r', 'y', 'o', 'w' };
            char[] strings4 = { 't', 't', 'e', 's', 't', 'u' };

            Console.WriteLine(removeWords.RemoveLetters(strings1, "string"));
            Console.WriteLine(removeWords.RemoveLetters(strings2, "balloon"));
            Console.WriteLine(removeWords.RemoveLetters(strings3, "norway"));
            Console.WriteLine(removeWords.RemoveLetters(strings4, "testing"));


        }
    }
}
