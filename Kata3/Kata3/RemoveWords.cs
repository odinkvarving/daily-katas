﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata3
{
    public class RemoveWords
    {

        public RemoveWords()
        {

        }
        public char[] RemoveLetters(char[] chars, string word)
        {
            string resultString;
            List<char> list = new List<char>(chars);
            foreach (char c in word)
            {
                list.Remove(c);
            }
            resultString = new string(list.ToArray());

            return resultString.ToCharArray(); ;
        }
    }
}
