﻿namespace Kata13
{
    public class Program
    {

        static void Main(string[] args)
        {
            Chessboard chessboard = new Chessboard();
            Console.WriteLine(chessboard.CannotCapture(new int[,] {
              { 0, 0, 0, 1, 0, 0, 0, 0 },
              { 0, 0, 0, 0, 0, 0, 0, 0 },
              { 0, 1, 0, 0, 0, 1, 0, 0 },
              { 0, 0, 0, 0, 1, 0, 1, 0 },
              { 0, 1, 0, 0, 0, 1, 0, 0 },
              { 0, 0, 0, 0, 0, 0, 0, 0 },
              { 0, 1, 0, 0, 0, 0, 0, 1 },
              { 0, 0, 0, 0, 1, 0, 0, 0 }
            }));

            Console.WriteLine(chessboard.CannotCapture(new int[,] {
              {1, 0, 1, 0, 1, 0, 1, 0},
              {0, 1, 0, 1, 0, 1, 0, 1},
              {1, 0, 1, 0, 1, 0, 1, 0},
              {0, 0, 0, 1, 0, 1, 0, 1},
              {1, 0, 0, 0, 1, 0, 1, 0},
              {0, 0, 0, 0, 0, 1, 0, 1},
              {1, 0, 1, 0, 1, 0, 1, 0},
              {1, 0, 0, 1, 0, 0, 0, 1}
            }));
        }
    }
}