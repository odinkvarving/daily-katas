﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata13
{
    internal class Chessboard
    {
        public bool CannotCapture(int[,] chessboard)
        {
            bool cannotCapture = true;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (chessboard[i, j] == 1)
                    {
                        cannotCapture = CheckPossibleMoves(chessboard, j, i);
                    }

                    if (!cannotCapture)
                    {
                        return false;
                    }
                }
            }
            return cannotCapture;
        }

        public bool CheckPossibleMoves(int[,] chessboard, int p, int q)
        {
            int[] Y = { 2, 1, -1, -2,
                   -2, -1, 1, 2 };

            int[] X = { 1, 2, 2, 1,
                   -1, -2, -2, -1 };

            for (int i = 0; i < 8; i++)
            {
                int y = q + Y[i];
                int x = p + X[i];

                if (x >= 0 && y >= 0 && x < 8 && y < 8)
                {
                    if (EncountersKnight(chessboard, y, x))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool EncountersKnight(int[,] chessboard, int y, int x)
        {
            if (chessboard[y, x] == 1)
            {
                return true;
            }
            return false;
        }
    }
}
