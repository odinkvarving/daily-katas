﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata9
{
    public static class ValidCreditData
    {
        public static bool ValidateCard(string cardNumber)
        {
            List<char> numbers = new List<char>(cardNumber.ToList());
            List<int> resultList = new List<int>();

            foreach (char c in numbers)
            {
                resultList.Add(c - '0');
            }

            if (resultList.Count < 14 || resultList.Count > 19)
            {
                return false;
            }

            int checkDigit = resultList.LastOrDefault();
            resultList.RemoveAt(resultList.Count - 1);
            resultList = Enumerable.Reverse(resultList).ToList();
            resultList = SelectiveDouble(resultList);

            return CheckSumDigit(resultList, checkDigit);
        }

        public static List<int> SelectiveDouble(List<int> cardNumber)
        {

            List<int> resultList = new List<int>();
            for (int i = 0; i < cardNumber.Count; i++)
            {
                if ((i + 1) % 2 != 0 || i == 0)
                {
                    int doubledNumber = (cardNumber[i] * 2);

                    if (doubledNumber >= 10)
                    {
                        char[] chars = doubledNumber.ToString().ToArray();


                        resultList.Add((chars[0] - '0') + (chars[1] - '0'));
                    }
                    else
                    {
                        resultList.Add(doubledNumber);
                    }
                }
                else
                {
                    resultList.Add(cardNumber[i]);
                }
            }
            return resultList;
        }

        public static bool CheckSumDigit(List<int> cardNumber, int checkDigit)
        {
            int sum = 0;

            foreach (int i in cardNumber)
            {
                sum += i;
            }

            char[] chars = sum.ToString().ToArray();

            int lastDigit = chars[1] - '0';

            if ((10 - lastDigit) == checkDigit)
            {
                return true;
            }
            return false;
        }
    }
}
