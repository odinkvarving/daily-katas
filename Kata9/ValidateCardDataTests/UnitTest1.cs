using Kata9;

namespace ValidateCardDataTests
{
    public class UnitTest1
    {

        [Fact]
        public void Test1()
        {
            Assert.False(ValidCreditData.ValidateCard("79927398714"));
        }



        [Fact]
        public void Test2()
        {
            Assert.False(ValidCreditData.ValidateCard("79927398713"));
        }



        [Fact]
        public void Test3()
        {
            Assert.True(ValidCreditData.ValidateCard("709092739800713"));
        }



        [Fact]
        public void Test4()
        {
            Assert.False(ValidCreditData.ValidateCard("1234567890123456"));
        }



        [Fact]
        public void Test5()
        {
            Assert.True(ValidCreditData.ValidateCard("12345678901237"));
        }



        [Fact]
        public void Test6()
        {
            Assert.True(ValidCreditData.ValidateCard("5496683867445267"));
        }



        [Fact]
        public void Test7()
        {
            Assert.False(ValidCreditData.ValidateCard("4508793361140566"));
        }



        [Fact]
        public void Test8()
        {
            Assert.True(ValidCreditData.ValidateCard("376785877526048"));
        }



        [Fact]
        public void Test9()
        {
            Assert.False(ValidCreditData.ValidateCard("36717601781975"));
        }
    }
}