﻿namespace Kata7
{
    public class Program
    {
        static void Main(string[] args)
        {
            GaussAddition gaussAddition = new GaussAddition();

            Console.WriteLine(gaussAddition.Gauss(0));
            Console.WriteLine(gaussAddition.Gauss(1, 0));
        }
    }
}
