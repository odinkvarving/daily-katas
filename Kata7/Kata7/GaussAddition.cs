﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata7
{
    public class GaussAddition
    {
        public int Gauss(int n, int m)
        {
            return (((Math.Abs(m - n) + 1) * (n + m)) / 2);
        }

        public int Gauss(int n)
        {
            //return Gauss(1, n);
            return (n * (n + 1)) / 2;
        }
    }


}
