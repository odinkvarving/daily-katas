using Kata7;
namespace GaussAdditionTests
{
    public class GaussAdditionTests
    {
        [Fact]
        public void Gauss_TestGaussWithOneNumber_ShouldReturn5050()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(100);

            Assert.Equal(5050, answer);
        }

        [Fact]
        public void Gauss_TestGaussWith5001And7000_ShouldReturn12001000()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(5001, 7000);

            Assert.Equal(12001000, answer);
        }

        [Fact]
        public void Gauss_TestGaussWith1975And165_ShouldReturn1937770()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(1975, 165);

            Assert.Equal(1937770, answer);
        }

        [Fact]
        public void Gauss_TestGaussWith5And5_ShouldReturn5()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(5, 5);

            Assert.Equal(5, answer);
        }

        [Fact]

        public void Gauss_TestGaussWith0_ShouldReturn0()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(0);

            Assert.Equal(0, answer);
        }

        [Fact]
        public void Gauss_TestGaussWith1And0_ShouldReturn1()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(1, 0);

            Assert.Equal(1, answer);
        }

        [Fact]
        public void Gauss_TestGaussWithMinus5And10_ShouldReturn40()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(-5, 10);

            Assert.Equal(40, answer);
        }

        [Fact]
        public void Gauss_TestGaussWithMinus10And5_ShouldReturnMinus40()
        {
            GaussAddition gaussAddition = new GaussAddition();

            int answer = gaussAddition.Gauss(-10, 5);

            Assert.Equal(-40, answer);
        }
    }
}