﻿namespace Kata8
{
    public class Program
    {
        static void Main(string[] args)
        {
            TemperatureConverter temperatureConverter = new TemperatureConverter();
            Console.WriteLine(temperatureConverter.Convert("-35*C"));
        }
    }
}
