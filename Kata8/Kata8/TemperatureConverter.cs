﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kata8
{
    internal class TemperatureConverter
    {
        public string Convert(string temperature)
        {
            string[] str = temperature.Split('*');
            int degrees = Int32.Parse(str[0]);
            string conversionResult = temperature.EndsWith('C') ? ((int)(degrees * (1.8D)) + 32).ToString() + "*F" : ((int)((degrees - 32) / 1.8D)).ToString() + "*C";
            return !temperature.EndsWith('C') && !temperature.EndsWith('F') ? "Error"
                : conversionResult;
        }
    }
}
