﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata6
{
    internal class PigLatin
    {

        public char[] consonants = new char[] { 'B', 'C', 'D', 'F', 'G', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'S', 'T', 'V', 'X', 'Z' };
        public char[] vowels = new char[] { 'A', 'E', 'I', 'O', 'U', 'W', 'Y' };
        public void TranslateWord(string word)
        {
            LinkedList<char> wordChars = new LinkedList<char>(word.ToCharArray());
            LinkedList<char> consonantsInWord = new LinkedList<char>();
            LinkedList<char> resultSentence = new LinkedList<char>();

            for (int i = 0; i < wordChars.Count; i++)
            {
                if (consonants.Contains(Char.ToUpper(wordChars.FirstOrDefault())))
                {
                    consonantsInWord.AddLast(wordChars.FirstOrDefault());
                    wordChars.Remove(wordChars.FirstOrDefault());
                }
            }

            for (int i = 0; i < wordChars.Count; i++)
            {
                if (!vowels.Contains(Char.ToUpper(wordChars.FirstOrDefault())))
                {
                    wordChars.Remove(wordChars.FirstOrDefault());
                    resultSentence.AddLast(wordChars.FirstOrDefault());
                }
                else
                {
                    wordChars.Remove(wordChars.FirstOrDefault());
                }
            }
                
            foreach (char c in consonantsInWord)
            {
                resultSentence.AddLast(c);
            }
        }
    }
}


