﻿namespace Kata1
{
    public class Program
    {
        static void Main(string[] args)
        {
            static bool IsParcelTongue(string sentence)
            {
                string[] arr = sentence.Split(' ');
                for(int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].ToLower().Contains('s'))
                    {
                        if (!arr[i].ToLower().Contains("ss"))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }

            static bool IsParcelTongueLambda(string text)
            {
                string[] words = text.ToLowerInvariant().Split(' ');
                bool res = true;
                Array.ForEach(words, w => { if(w.Contains('s') && !w.Contains("ss")) { res = false; } });
                return res;
            }


            string sentence= "Sshe ssselects to eat that apple. ";
            string sentence1 = "She ssselects to eat that apple. ";
            string sentence2 = "You ssseldom sssspeak sso boldly, ssso messmerizingly. ";
            string sentence3 = "Steve likes to eat pancakes. ";
            string sentence4 = "Sssteve likess to eat pancakess. ";
            string sentence5 = "Beatrice samples lemonade. ";
            string sentence6 = "Beatrice enjoysss lemonade. ";

            Console.WriteLine(IsParcelTongue(sentence));
            Console.WriteLine(IsParcelTongue(sentence2));
            Console.WriteLine(IsParcelTongue(sentence3));
            Console.WriteLine(IsParcelTongue(sentence4));
            Console.WriteLine(IsParcelTongue(sentence5));
            Console.WriteLine(IsParcelTongue(sentence6));

            Console.WriteLine(IsParcelTongueLambda(sentence));






        }
    }

}

