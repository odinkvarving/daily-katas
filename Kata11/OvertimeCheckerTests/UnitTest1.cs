using Kata11;

namespace OvertimeCheckerTests
{
    public class UnitTest1
    {

        [Fact]
        public void Overtime_CheckOnlyRegularHours_ShouldBe240()
        {
            OvertimeChecker overtimeChecker = new OvertimeChecker();

            string result = overtimeChecker.Overtime(9, 17, 30, 1.5);

            Assert.Equal("$240", result);
        }

        [Fact]
        public void Overtime_CheckPartialOvertimeHours_ShouldBe84()
        {
            OvertimeChecker overtimeChecker = new OvertimeChecker();

            string result = overtimeChecker.Overtime(16, 18, 30, 1.8);

            Assert.Equal("$84", result);
        }

        [Fact]
        public void Overtime_CheckOnlyRegularWithDecimal_ShouldBe52Point5()
        {
            OvertimeChecker overtimeChecker = new OvertimeChecker();

            string result = overtimeChecker.Overtime(13.25, 15, 30, 1.5);

            Assert.Equal("$52,5", result);
        }
    }
}