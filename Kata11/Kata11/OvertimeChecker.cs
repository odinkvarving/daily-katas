﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata11
{
    public class OvertimeChecker
    {
        public string Overtime(double start, double end, int hourlyRate, double overtimeMultiplier)
        {
            if (TimeSpan.FromHours(start) > TimeSpan.FromHours(end))
            {
                return "Invalid worktime";
            }

            if (TimeSpan.FromHours(end) > TimeSpan.FromHours(17))
            {
                double regularHours = (TimeSpan.FromHours(17) - TimeSpan.FromHours(start)).TotalHours * hourlyRate;
                double overtimeHours = ((TimeSpan.FromHours(end) - TimeSpan.FromHours(17)).TotalHours * hourlyRate) * overtimeMultiplier;

                return $"${regularHours + overtimeHours}";
            }
            else
            {
                double regularHours = (TimeSpan.FromHours(end) - TimeSpan.FromHours(start)).TotalHours * hourlyRate;

                return $"${regularHours}";
            }
        }


        public double OvertimeV2(double start, double end, int hourlyRate, double overtimeMultiplier)
        {
            return ((TimeSpan.FromHours(17) - TimeSpan.FromHours(start)).TotalHours > 0.0) ? (TimeSpan.FromHours(17) - TimeSpan.FromHours(start)).TotalHours * hourlyRate + ((TimeSpan.FromHours(end) - TimeSpan.FromHours(17)).TotalHours * hourlyRate) * overtimeMultiplier : 0.0;
        }

    }
}
