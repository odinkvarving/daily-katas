﻿namespace Kata11
{
    public class Program
    {
        static void Main(string[] args)
        {
            OvertimeChecker checker = new OvertimeChecker();
            Console.WriteLine(checker.Overtime(9, 17, 30, 1.5));
            Console.WriteLine(checker.Overtime(16, 18, 30, 1.8));
            Console.WriteLine(checker.Overtime(13.25, 15, 30, 1.5));
            checker.Overtime(17, 9, 30, 1.5);
            Console.WriteLine(checker.OvertimeV2(9, 17, 30, 1.5)); 
        }
    }
}
