﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.Ingredients
{
    internal class Banana : Ingredient
    {
        public Banana()
        {
            Name = "Banana";
            Price = 0.75;
        }
    }
}
