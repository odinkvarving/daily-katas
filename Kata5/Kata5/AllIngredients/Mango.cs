﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.Ingredients
{
    internal class Mango : Ingredient
    {

        public Mango()
        {
            Name = "Mango";
            Price = 0.80;
        }
    }
}
