﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.Ingredients
{
    internal class Pineapple : Ingredient
    {
        public Pineapple()
        {
            Name = "Pineapple";
            Price = 0.80;
        }
    }
}
