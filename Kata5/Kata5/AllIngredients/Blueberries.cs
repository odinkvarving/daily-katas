﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.Ingredients
{
    internal class Blueberries : Ingredient
    {
        public Blueberries()
        {
            Name = "Blueberry";
            Price = 0.45;
        }

    }
}
