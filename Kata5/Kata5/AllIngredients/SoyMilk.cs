﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.AllIngredients
{
    internal class SoyMilk : Milk
    {
        public SoyMilk()
        {
            Name = "Soy milk";
            Vegan = true;
        }
    }
}
