﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.AllIngredients
{
    internal class OatMilk : Milk
    {
        public OatMilk()
        {
            Name = "Oat milk";
            Vegan = true;
        }
    }
}
