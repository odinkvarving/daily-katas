﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.AllIngredients
{
    internal class AlmondMilk : Milk
    {
        public AlmondMilk()
        {
            Name = "Almond milk";
            Vegan = true;
        }
    }
}
