﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.Ingredients
{
    internal class Apple : Ingredient
    {
        public Apple()
        {
            Name = "Apple";
            Price = 0.75;
        }
    }
}
