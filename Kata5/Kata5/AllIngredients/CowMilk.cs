﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.AllIngredients
{
    internal class CowMilk : Milk
    {
        public CowMilk()
        {
            Name = "Cow's milk";
            Vegan = false;
        }
    }
}
