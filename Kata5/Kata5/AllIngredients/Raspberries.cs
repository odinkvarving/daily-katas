﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5.Ingredients
{
    internal class Raspberries : Ingredient
    {
        public Raspberries()
        {
            Name = "Raspberry";
            Price = 0.55;
        }
    }
}
