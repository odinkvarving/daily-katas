﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5
{
    public class Smoothie
    {
        public List<Ingredient> Ingredients { get; set; }

        public Milk Milk { get; set; }

        public double? cost;

        public double? price;

        public string? name;

        public Smoothie(List<Ingredient> ingredients, Milk milk)
        {
            Ingredients = ingredients;
            Milk = milk;
        }

        public double GetCost()
        {
            double costOfIngredients = 0;
            foreach (Ingredient ingredient in Ingredients)
            {
                costOfIngredients += ingredient.Price;
            }
            string costString = "£" + costOfIngredients.ToString();
            Console.WriteLine(costString);
            this.cost = costOfIngredients;
            return costOfIngredients;
        }

        public double GetPrice()
        {
            double price = (double)(cost + (this.cost * 1.5));

            string priceString = "£" + price.ToString();
            Console.WriteLine(priceString);
            return price;
        }

        public string GetName()
        {
            List<Ingredient> sortedList = Ingredients.OrderBy(ingredient => ingredient.Name).ToList();
            StringBuilder sb = new StringBuilder();

            int counter = sortedList.Count;
            foreach (Ingredient ingredient in sortedList)
            {
                sb.Append(ingredient.Name + " ");
            }

            if (counter >= 2)
            {
                sb.Append("Fusion");
            }
            else
            {
                sb.Append(" Smoothie");
            }
            if (Milk.Vegan == true)
            {
                sb.Append(" (VGN)");
            }

            return sb.ToString();
        }
    }
}
