﻿using Kata5.AllIngredients;
using Kata5.Ingredients;
namespace Kata5
{
    public class Program
    {
        static void Main(string[] args)
        {
            Smoothie smoothie1 = new Smoothie(new List<Ingredient>() { new Strawberries(), new Banana() }, new AlmondMilk());

            Console.WriteLine(smoothie1.GetName());
            smoothie1.GetCost();
            smoothie1.GetPrice();
        }
    }
}
