﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5
{
    public abstract class Milk
    {
        public string Name { get; set; }

        public bool Vegan { get; set; }
    }
}
