﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata5
{
    public abstract class Ingredient
    {
        public string Name { get; set; }

        public double Price { get; set; }

    }


}
