﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata10
{
    public static class PalindromeTimeStamps
    {

        public static int PalendromeTimestamps(int fromHours, int fromMinutes, int fromSeconds, int toHours, int toMinutes, int toSeconds)
        {

            TimeSpan start = new TimeSpan(fromHours, fromMinutes, fromSeconds);
            TimeSpan end = new TimeSpan(toHours, toMinutes, toSeconds);
            int counter = 0;

            while (start <= end)
            {

                string checkString = GetFormattedStrings(start.Hours, start.Minutes, start.Seconds);
                if (IsPalindrome(checkString))
                {
                    counter++;
                }

                start = start.Add(TimeSpan.FromSeconds(1));
            }

            Console.WriteLine(counter);
            return counter;
        }

        public static string Pad(int N)
        {
            string number = N.ToString();
            number = number.PadLeft(2, '0');
            return number;
        }

        public static string GetFormattedStrings(int hours, int minutes, int seconds)
        {
            string hoursString = "";
            string minutesString = "";
            string secondsString = "";


            if (hours > 10)
            {
                hoursString = hours.ToString();
            }
            else
            {
                hoursString = Pad(hours);
            }

            if (minutes >= 10)
            {
                minutesString = minutes.ToString();
            }
            else
            {
                minutesString = Pad(minutes);
            }

            if (seconds >= 10)
            {
                secondsString = seconds.ToString();
            }
            else
            {
                secondsString = Pad(seconds);
            }

            return hoursString + minutesString + secondsString;
        }

        public static bool IsPalindrome(string timeStamp)
        {
            return timeStamp == Reverse(timeStamp);
        }
        static string Reverse(string str)
        {
            return new string(str.ToCharArray().Reverse().ToArray());
        }
    }
}
