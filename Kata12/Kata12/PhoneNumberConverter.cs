﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata12
{

    internal class PhoneNumberConverter
    {
        public string TextToNum(string numberText)
        {
            Dictionary<int, string> keyValuePairs = new Dictionary<int, string>() { { 0, "" }, { 1, "" }, { 2, "ABC" }, { 3, "DEF" }, { 4, "GHI" }, { 5, "JKL" }, { 6, "MNO" }, { 7, "PQRS" }, { 8, "TUV" }, { 9, "WXYZ" } };

            var pattern = @"\((?<AreaCode>\d{3})\)\s*(?<Number>\d{3}(?:-|\s*)\d{4})";
            var regexp = new System.Text.RegularExpressions.Regex(pattern);
            if (regexp.IsMatch(numberText))
            {
                string[] first = numberText.Split("-");
                string[] second = first[0].Split(" ");
                string[] number = new string[] { second[0], second[1], first[1] };
                return convert(number, keyValuePairs);
            }

            else
            {
                string[] number = numberText.Split("-");
                return convert(number, keyValuePairs);
            }
        }

        public string replaceChars(Dictionary<int, string> dict, string segment)
        {
            string newNumber = "";
            foreach (char c in segment)
            {
                var thisDict = dict.FirstOrDefault(t => t.Value.Contains(c, StringComparison.CurrentCultureIgnoreCase));
                newNumber += thisDict.Key.ToString();
            }

            return newNumber;
        }

        public string convert(string[] number, Dictionary<int, string> dict)
        {
            if (number[0].Any(x => char.IsLetter(x)))
            {
                number[0] = replaceChars(dict, number[0]);
            }

            if (number[1].Any(x => char.IsLetter(x)))
            {
                number[1] = replaceChars(dict, number[1]);
            }

            if (number[2].Any(x => char.IsLetter(x)))
            {
                number[2] = replaceChars(dict, number[2]);
            }

            if (number[0].Contains("("))
            {
                return number[0] + " " + number[1] + "-" + number[2];
            }
            else
            {
                return string.Join("-", number);
            }
        }
    }
}
