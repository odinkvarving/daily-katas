﻿namespace Kata12
{
    public class Program
    {
        static void Main(string[] args)
        {
            PhoneNumberConverter converter = new PhoneNumberConverter();
            Console.WriteLine(converter.TextToNum("123-647-EYES"));
            Console.WriteLine(converter.TextToNum("(325) 444-8378"));
            Console.WriteLine(converter.TextToNum("tIn-AnD-gOlD"));
        }
    }
}
